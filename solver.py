#!/usr/bin/python3

import sys
import json

def get_best_score(dico):
    best_score = 0
    best_word = ""
    for rec in dico:
        for word in rec: #dirty
            if rec[word] > best_score:
                best_word = word
                best_score = rec[word]
    return best_word, best_score

def answer(best_word, best_score):
    print("\nThe best word to try is `{}` with a score of {}".format(best_word.upper(), best_score))

def try_word(tried_word, dico_f):
    result = input("What is your result ? ($ / * / - )\n")
    if not ("*" in result or "-" in result):
        # Word was found, exit
        return 0

    #Initialize empty filters
    found_letters = {}
    missplaced_letters = {}
    wrong_letters = []

    #Fill filters 
    for i,r in enumerate(result):
        letter = tried_word[i]
        if r == "$":
            if not letter in found_letters:
                found_letters[letter] = []
            if not i in found_letters[letter]:
                found_letters[letter].append(i)
        if r == "*":
            if not letter in missplaced_letters:
                missplaced_letters[letter] = []
            if not i in missplaced_letters[letter]:
                missplaced_letters[letter].append(i)
        if r == "-":
            if (not letter in found_letters) and (not letter in missplaced_letters) and (not letter in missplaced_letters): # Could be better
                wrong_letters.append(letter)

    # print(found_letters)
    # print(missplaced_letters)
    # for letter in missplaced_letters:
    #     print(letter)
    # print(wrong_letters)

    # Generate a new filtered dictionnary
    dico_f2 = []
    for rec in dico_f:
        valid_word = True
        for word in rec: #dirty
            for letter in wrong_letters:
                if letter in word:
                    valid_word = False
                    print("(WL) Rejected {} because it contains letter {}".format(word,letter))
                    break
            
            if not valid_word:
                break
                    
            for letter in missplaced_letters:
                if not letter in word:
                    valid_word = False
                    print("(ML) Rejected {} because it does not contain letter {}".format(word,letter))
                    break
                for i in missplaced_letters[letter]:
                    if word[i] == letter:
                        valid_word = False
                        print("(ML) Rejected {} because it contains letter {} in position {}".format(word,letter, i+1))
                        break
                if not valid_word:
                    break
                tmpList = list(word)
                for i in sorted(missplaced_letters[letter], reverse=True):
                    del(tmpList[i])
                tmpWord = "".join(tmpList)
                if not letter in tmpWord:
                    valid_word = False
                    print("(ML) Rejected {} because it does not contain letter {} in a position that has not been eliminated".format(word,letter))
                    break

            if not valid_word:
                break
            
            for letter in found_letters:
                if not letter in word:
                    valid_word = False
                    print("(FL) Rejected {} because it does not contain letter {}".format(word,letter))
                    break
                for i in found_letters[letter]:
                    if word[i] != letter:
                        valid_word = False
                        print(f"(FL) Rejected {word} because it does not contain letter {letter} at position {i+1}")
                        break
                if not valid_word:
                    break

        if valid_word:
            print("Found no reason to eliminate `{}`, keeping it as a candidate".format(word))
            dico_f2.append(rec)
    
    print("\nCandidates :")
    for rec in dico_f2:
        for word in rec:
            print("{} -> {}".format(word, rec[word]))
    #print(dico_f2)

    # Get score, and recursively call try_word() with the new answer
    best_word, best_score = get_best_score(dico_f2)
    answer(best_word, best_score)
    try_word(best_word, dico_f2)


def main():

    # Parameters for initial filter
    nb_letters = input("How many letters are in the word ? ")
    first_letter = input("What's the word first letter ? ").upper()

    with open("dico_scored.json", "r") as f:
        dico = json.load(f)
    dico = dico[nb_letters]

    # Generate the first filtered dictionnary
    dico_f1 = []
    for rec in dico:
        for word in rec: #dirty
            if word.startswith(first_letter):
                dico_f1.append(rec)

    best_word, best_score = get_best_score(dico_f1)

    answer(best_word, best_score)
    try_word(best_word, dico_f1)
    print("Done!")

if (__name__=='__main__'):
    sys.exit(main())