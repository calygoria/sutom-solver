#!/usr/bin/python3

import csv
import unicodedata
import json

# Clean input dictionnary
alphabet = "abcdefghijklmnopqrstuvwxyz".upper()

dico_cleaned = []

with open("ListeMotsProposables.txt", "r") as dico:
    for line in dico:
        if line.startswith('    "'):
            word = line.replace(' ', '').replace(',', '').replace('"', '')
            dico_cleaned.append(word)

# Save cleaned dataset
with open("dico_cleaned.csv", "w", encoding="UTF-8", newline='\n') as f:
    for word in sorted(list(set(dico_cleaned))):
        f.write(word)

# Frequency analysis of cleaned dictionnary
count = {}
freq = {}
total = 0
for l in alphabet:
    count[l] = 0
    freq[l] = 0

for word in dico_cleaned:
    word = word.rstrip('\n')
    for l in word:
        count[l] = count[l] + 1
        total = total + 1

for l in alphabet:
    freq[l] = round((count[l] / total)*100, 2)

# for letter, value in freq.items():
#     print("{} -> {}%".format(letter, value))

with open("freq.json", "w") as f:
    f.write(json.dumps(freq, indent=4, sort_keys=True))


# Score each word of dictionnary with freq average, NOT COUNTING FIRST LETTER (cause sutom), not counting repeats
dico_scored = {}
for word in dico_cleaned:
    word = word.rstrip('\n')
    length = len(word)
    score = 0
    for l in "".join(set(word[1:])):
        #print("{} as a score of {}".format(l, freq[l]))
        score = score + freq[l]
    score = round(score / length, 2)
    #print("{} as a score of {}".format(word, score))
    if not length in dico_scored:
        dico_scored[length] = []
    dico_scored[length].append({word: score})

#print(json.dumps(dico_scored, indent=4))

with open("dico_scored.json", "w") as f:
    f.write(json.dumps(dico_scored, indent=4, sort_keys=True))