# Quickstart

Regenerate dictionnary and frequency analysis :

```bash
python ./data_prep.py
```

Run the solver :

```bash
python ./solver.py
```

For each try, enter :
- `$` for a good character well placed
- `*` for a good character miss placed
- `-` for a wrong character